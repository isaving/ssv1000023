package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRSH0I struct {
	AcctgNo		string ` validate:"required,max=20"`
	AcctgAcctNo	string ` validate:"required,max=20"`
}

type DAACRSH0O struct {
	AcctgNo            string
	AcctgAcctNo        string
	ProdType           string
	AcctType           string
	Lvls               float64
	Sigma              float64
	IntRate            string
	IntPlanNo          string
	AcctBal            string
	CumulativeProdAmt  float64
	StartDate          string
	AsOfDate           string
	RestCycleStartDate string
	LastMaintDate      string
	LastMaintTime      string
	LastMaintBrno      string
	LastMaintTell      string

}

type DAACRSH0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRSH0I
}

type DAACRSH0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRSH0O
}

type DAACRSH0RequestForm struct {
	Form []DAACRSH0IDataForm
}

type DAACRSH0ResponseForm struct {
	Form []DAACRSH0ODataForm
}

// @Desc Build request message
func (o *DAACRSH0RequestForm) PackRequest(DAACRSH0I DAACRSH0I) (responseBody []byte, err error) {

	requestForm := DAACRSH0RequestForm{
		Form: []DAACRSH0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRSH0I",
				},
				FormData: DAACRSH0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRSH0RequestForm) UnPackRequest(request []byte) (DAACRSH0I, error) {
	DAACRSH0I := DAACRSH0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRSH0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRSH0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRSH0ResponseForm) PackResponse(DAACRSH0O DAACRSH0O) (responseBody []byte, err error) {
	responseForm := DAACRSH0ResponseForm{
		Form: []DAACRSH0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRSH0O",
				},
				FormData: DAACRSH0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRSH0ResponseForm) UnPackResponse(request []byte) (DAACRSH0O, error) {

	DAACRSH0O := DAACRSH0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRSH0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRSH0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRSH0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
