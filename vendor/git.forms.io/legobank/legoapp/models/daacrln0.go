package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRLN0I struct {
	AcctgAcctNo		string ` validate:"required,max=32"`
}

type DAACRLN0O struct {
	AcctCreateDate   interface{} `json:"AcctCreateDate"`
	AcctType         interface{} `json:"AcctType"`
	AcctgAcctNo      string      `json:"AcctgAcctNo"`
	AcctingOrgID     interface{} `json:"AcctingOrgId"`
	Balance          string      `json:"Balance"`
	BalanceType      string      `json:"BalanceType"`
	BalanceYestoday  interface{} `json:"BalanceYestoday"`
	BtBalTemp        interface{} `json:"BtBalTemp"`
	ContID           interface{} `json:"ContID"`
	Currency         interface{} `json:"Currency"`
	CurrencyMarket   interface{} `json:"CurrencyMarket"`
	CustID           interface{} `json:"CustID"`
	Interest         interface{} `json:"Interest"`
	InterestRate     string      `json:"InterestRate"`
	InterestYestoday interface{} `json:"InterestYestoday"`
	IsCalInt         interface{} `json:"IsCalInt"`
	LastCalcIntdate  interface{} `json:"LastCalcIntdate"`
	LastMaintBrno    interface{} `json:"LastMaintBrno"`
	LastMaintDate    interface{} `json:"LastMaintDate"`
	LastMaintTell    interface{} `json:"LastMaintTell"`
	LastMaintTime    interface{} `json:"LastMaintTime"`
	LastTranDate     string      `json:"LastTranDate"`
	LastTranIntdate  interface{} `json:"LastTranIntdate"`
	MgmtOrgID        interface{} `json:"MgmtOrgId"`
	OrgCreate        interface{} `json:"OrgCreate"`
	ProdCode         interface{} `json:"ProdCode"`
	Status           interface{} `json:"Status"`
	TccState         int         `json:"TccState"`
	TotIntReceived   interface{} `json:"TotIntReceived"`
}

type DAACRLN0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRLN0I
}

type DAACRLN0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRLN0O
}

type DAACRLN0RequestForm struct {
	Form []DAACRLN0IDataForm
}

type DAACRLN0ResponseForm struct {
	Form []DAACRLN0ODataForm
}

// @Desc Build request message
func (o *DAACRLN0RequestForm) PackRequest(DAACRLN0I DAACRLN0I) (responseBody []byte, err error) {

	requestForm := DAACRLN0RequestForm{
		Form: []DAACRLN0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLN0I",
				},
				FormData: DAACRLN0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRLN0RequestForm) UnPackRequest(request []byte) (DAACRLN0I, error) {
	DAACRLN0I := DAACRLN0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLN0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLN0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRLN0ResponseForm) PackResponse(DAACRLN0O DAACRLN0O) (responseBody []byte, err error) {
	responseForm := DAACRLN0ResponseForm{
		Form: []DAACRLN0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLN0O",
				},
				FormData: DAACRLN0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRLN0ResponseForm) UnPackResponse(request []byte) (DAACRLN0O, error) {

	DAACRLN0O := DAACRLN0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLN0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLN0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRLN0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
