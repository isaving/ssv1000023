package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL1NM013I struct {
	AcctiAcctNo       string `Required;valid:"MaxSize(32)"` // 贷款核算账号
	ListArray []ListArray
}

type ListArray struct {
	IntrPlanTypCd    	 string `Required;valid:"MaxSize(2)"` // 利息计息类型代码
	OrgNo    	  		 string `Required;valid:"MaxSize(4)"` // 机构号
	ValidFlg    	  	 string `valid:"MaxSize(1)"` // 有效标志
	IntacrRuleNo       	 string `Required;valid:"MaxSize(40)"`  // 计息规则编号
}


type IL1NM013O struct {
	TxDt	string
	TxSn	string
}

type IL1NM013IDataForm struct {
	FormHead CommonFormHead
	FormData IL1NM013I
}

type IL1NM013ODataForm struct {
	FormHead CommonFormHead
	FormData IL1NM013O
}

type IL1NM013RequestForm struct {
	Form []IL1NM013IDataForm
}

type IL1NM013ResponseForm struct {
	Form []IL1NM013ODataForm
}

// @Desc Build request message
func (o *IL1NM013RequestForm) PackRequest(IL1NM013I IL1NM013I) (responseBody []byte, err error) {

	requestForm := IL1NM013RequestForm{
		Form: []IL1NM013IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL1NM013I",
				},
				FormData: IL1NM013I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL1NM013RequestForm) UnPackRequest(request []byte) (IL1NM013I, error) {
	IL1NM013I := IL1NM013I{}
	if err := json.Unmarshal(request, o); nil != err {
		return IL1NM013I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL1NM013I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL1NM013ResponseForm) PackResponse(IL1NM013O IL1NM013O) (responseBody []byte, err error) {
	responseForm := IL1NM013ResponseForm{
		Form: []IL1NM013ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL1NM013O",
				},
				FormData: IL1NM013O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL1NM013ResponseForm) UnPackResponse(request []byte) (IL1NM013O, error) {

	IL1NM013O := IL1NM013O{}

	if err := json.Unmarshal(request, o); nil != err {
		return IL1NM013O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL1NM013O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL1NM013I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
