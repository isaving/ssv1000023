package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAB00004I struct {
	Id         int
	ProjId     int
	FlowId     string
	ExecId     int
	JobId      string
	ReplyTopic string
	RetCode    string
	RetMsg     string

	PubMap		map[string]string
	EnvMap		map[string][]string
	ArrayMap	map[string][]string
}

type EnvMap struct {
	BIZ_AccountingDate		string `json:"BIZ_AccountingDate";valid:Required`	//当前会计日
	BIZ_CurrentDate			string `json:"BIZ_CurrentDate,omitempty"`   		//当前批处理日期
}

type DAB00004O struct {
	//TODO add output fields
}

// @Desc Build request message
func (o *DAB00004I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *DAB00004I) UnPackRequest(requestBody []byte) (err error) {
	//mapOne := make(map[string]interface{})
	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *DAB00004O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAB00004O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *DAB00004I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

