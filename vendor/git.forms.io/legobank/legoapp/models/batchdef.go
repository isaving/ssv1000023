package models

type BatchDefine struct {
	Id         int
	ProjId     int
	FlowId     string
	ExecId     int
	JobId      string
	ReplyTopic string
	RetCode    string
	RetMsg     string
	PubMap     map[string]string
	EnvMap     map[string]string
	ArrayMap   map[string][]string
}

type BatchArgs struct {
	PubMap   map[string]string
	EnvMap   map[string]string
	ArrayMap map[string][]string
}

type ReplyBody struct {
	ReturnCode string      `json:"returnCode"`
	ReturnMsg  string      `json:"ReturnMsg"`
	Data       interface{} `json:"data"`
}



