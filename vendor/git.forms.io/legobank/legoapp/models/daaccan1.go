package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCAN1I struct {
	AcctgAcctNo      string ` validate:"required,max=32"`
	Currency         string
	AcctStatus       string
	CustId           string
	ContId           string
	ProdCode         string
	OrgCreate        string
	AcctCreateDate   string
	MaturityDate     string
	DrawDate         string
	DrawTotBalance   float64
	RepPrinTotTerm   int64
	LoanDubilNo		 string
	CurrRepPrinTerm  int64
	NextPrinDate     string
	RepayType        string
	RepayFreq        string
	Balance          float64
	BalanceYesterday float64
	UnpaidBal        float64
	CavAmt           float64
	LastTranDate     string
	ValidStatus      string
	MgmtOrgId        string
	AcctingOrgId     string
	LastMaintDate    string
	LastMaintTime    string
	LastMaintBrno    string
	LastMaintTell    string
	Status           string
	TccState         int
	RepayBankFlag	string	//还款账号本他行标志
	RepayBankNo		string 	//还款账号行号
	RepayBankName	string	//还款账号银行名称
	RepayAcctNo		string	//还款账号
	RepayAcctName	string	//还款账号户名


	TxAcctnDt                 string
	TxTm                      string
	HostTranSerialNo          string
	HostTranSeq               string
	HostTranInq               string
	BookedWorkday             string
	AcctingTime               string
	LiquidationDate           string
	LiquidationTime           string
	TranOrgId                 string
	AgentOrgId                string
	TranTeller                string
	AuthTeller                string
	ReviewTeller              string
	TranChannel               string
	AccessChannel             string
	TerminalNo                string
	BussSys                   string
	TrasactionCode            string
	ClearingBussType          string
	BussType                  string
	BussCategories            string
	DebitFlag                 string
	LocalBankFlag             string
	Country                   string
	OtherBankFlag             string
	LocalAcctDiff             string
	LocalTranDetailType       string
	TheMerchantNo             string
	BussAcctNo                string
	AcctNo                    string
	AmtType                   string
	BanknoteFlag              string
	Amt                       float64
	AcctBal                   float64
	CashTranFlag              string
	ValueDate                 string
	ExchRestType              string
	AntiTradingMark           string
	CorrectTradingFlag        string
	OriginalTradingDay        string
	OriginalHostTranSerialNo  string
	OriginalTradeTellerNo     string
	AcctManagerNo             string
	MessageCode               string
	AbstractCode              string
	UseCode                   string
	SetNo                     string
	WhetherToAllowPunching    string
	ChannelCorrectControlFlag string
	IsSendClearingFlag        string
	IsMakeUpForPayables       string
}

type DAACCAN1O struct {
	AcctStatus            string
	CustId                string
	ContId                string
	ProdCode              string
	OrgCreate             string
	AcctCreateDate        string
	MaturityDate          string
	DrawDate              string
	DrawTotBalance        float64
	RepPrinTotTerm        int64
	CurrRepPrinTerm       int64
	NextPrinDate          string
	RepayType             string
	RepayFreq             string
	Balance               float64
	BalanceYesterday      float64
	UnpaidBal             float64
	CavAmt                float64
	LastTranDate          string
	ValidStatus           string
	MgmtOrgId             string
	AcctingOrgId          string
	LastMaintDate         string
	LastMaintTime         string
	LastMaintBrno         string
	LastMaintTell         string
	BalanceTemp1          float64
	BalanceTemp2          float64
	BalanceYesterdayTemp1 float64
	BalanceYesterdayTemp2 float64
	TccState              int8
	Status                string

}

type DAACCAN1IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCAN1I
}

type DAACCAN1ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCAN1O
}

type DAACCAN1RequestForm struct {
	Form []DAACCAN1IDataForm
}

type DAACCAN1ResponseForm struct {
	Form []DAACCAN1ODataForm
}

// @Desc Build request message
func (o *DAACCAN1RequestForm) PackRequest(DAACCAN1I DAACCAN1I) (responseBody []byte, err error) {

	requestForm := DAACCAN1RequestForm{
		Form: []DAACCAN1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCAN1I",
				},
				FormData: DAACCAN1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCAN1RequestForm) UnPackRequest(request []byte) (DAACCAN1I, error) {
	DAACCAN1I := DAACCAN1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCAN1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCAN1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCAN1ResponseForm) PackResponse(DAACCAN1O DAACCAN1O) (responseBody []byte, err error) {
	responseForm := DAACCAN1ResponseForm{
		Form: []DAACCAN1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCAN1O",
				},
				FormData: DAACCAN1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCAN1ResponseForm) UnPackResponse(request []byte) (DAACCAN1O, error) {

	DAACCAN1O := DAACCAN1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCAN1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCAN1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCAN1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
