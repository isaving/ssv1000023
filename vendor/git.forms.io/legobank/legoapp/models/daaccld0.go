package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCLD0I struct {
	TxAcctnDt         string
	TxTm              string
	HostTranSerialNo  string
	HostTranSeq       string
	HostTranInq       string
	RcrdacctAcctnDt   string
	RcrdacctAcctnTm   string
	LiqdDt            string
	LiqdTm            string
	TxOrgNo           string
	AgenOrgNo         string
	TxTelrNo          string
	AuthTelrNo        string
	RchkTelrNo        string
	LunchChnlTypCd    string
	AccessChnlTypCd   string
	TerminalNo        string
	BizSysNo          string
	TranCd            string
	LiqdBizTypCd      string
	BizKindCd         string
	BizClsfCd         string
	DebitCrdtFlg      string
	OpenAcctOrgNo     string
	MbankAcctFlg      string
	CustNo            string
	SvngAcctNo        string
	CardNoOrAcctNo    string
	LoanAcctiAcctNo   string
	TermNo            string
	FinTxAmtTypCd     string
	CurCd             string
	TxnAmt            float64
	AcctBal           float64
	CashTxRcptpymtCd  string
	ValueDate         string
	FrnexcgStlManrCd  string
	PostvReblnTxFlgCd string
	RvrsTxFlgCd       string
	OrginlTxAcctnDt   string
	OrginlHostTxSn    string
	OrginlTxTelrNo    string
	CustMgrTelrNo     string
	MessageCode       string
	AbstractCode      string
	Abstract          string
	PmitRvrsFlg       string
	ChnlRvrsCtrlFlgCd string
	GvayLiqdFlg       string
	MndArapFlg        string
	LastMaintDate     string
	LastMaintTime     string
	LastMaintBrno     string
	LastMaintTell     string
	Status            string
	TccState          int
	StateAndRgnCd     string
	MbankFlg          string
	CashrmtFlgCd      string
	OthbnkBnkNo       string
	TxDtlTypCd        string
	MerchtNo          string
	AcctBookNo        string
	TermNumber        int
	LoanUsageCd       string
}

type DAACCLD0O struct {
	TxAcctnDt         string
	TxTm              string
	HostTranSerialNo  string
	HostTranSeq       string
	HostTranInq       string
	RcrdacctAcctnDt   string
	RcrdacctAcctnTm   string
	LiqdDt            string
	LiqdTm            string
	TxOrgNo           string
	AgenOrgNo         string
	TxTelrNo          string
	AuthTelrNo        string
	RchkTelrNo        string
	LunchChnlTypCd    string
	AccessChnlTypCd   string
	TerminalNo        string
	BizSysNo          string
	TranCd            string
	LiqdBizTypCd      string
	BizKindCd         string
	BizClsfCd         string
	DebitCrdtFlg      string
	OpenAcctOrgNo     string
	MbankAcctFlg      string
	CustNo            string
	SvngAcctNo        string
	CardNoOrAcctNo    string
	LoanAcctiAcctNo   string
	TermNo            string
	FinTxAmtTypCd     string
	CurCd             string
	TxnAmt            float64
	AcctBal           float64
	CashTxRcptpymtCd  string
	ValueDate         string
	FrnexcgStlManrCd  string
	PostvReblnTxFlgCd string
	RvrsTxFlgCd       string
	OrginlTxAcctnDt   string
	OrginlHostTxSn    string
	OrginlTxTelrNo    string
	CustMgrTelrNo     string
	MessageCode       string
	AbstractCode      string
	Abstract          string
	PmitRvrsFlg       string
	ChnlRvrsCtrlFlgCd string
	GvayLiqdFlg       string
	MndArapFlg        string
	LastMaintDate     string
	LastMaintTime     string
	LastMaintBrno     string
	LastMaintTell     string
	Status            string
	TccState          int
	StateAndRgnCd     string
	MbankFlg          string
	CashrmtFlgCd      string
	OthbnkBnkNo       string
	TxDtlTypCd        string
	MerchtNo          string
	AcctBookNo        string
	TermNumber        int
	LoanUsageCd       string
}

type DAACCLD0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCLD0I
}

type DAACCLD0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCLD0O
}

type DAACCLD0RequestForm struct {
	Form []DAACCLD0IDataForm
}

type DAACCLD0ResponseForm struct {
	Form []DAACCLD0ODataForm
}

// @Desc Build request message
func (o *DAACCLD0RequestForm) PackRequest(DAACCLD0I DAACCLD0I) (responseBody []byte, err error) {

	requestForm := DAACCLD0RequestForm{
		Form: []DAACCLD0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLD0I",
				},
				FormData: DAACCLD0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCLD0RequestForm) UnPackRequest(request []byte) (DAACCLD0I, error) {
	DAACCLD0I := DAACCLD0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLD0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLD0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCLD0ResponseForm) PackResponse(DAACCLD0O DAACCLD0O) (responseBody []byte, err error) {
	responseForm := DAACCLD0ResponseForm{
		Form: []DAACCLD0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLD0O",
				},
				FormData: DAACCLD0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCLD0ResponseForm) UnPackResponse(request []byte) (DAACCLD0O, error) {

	DAACCLD0O := DAACCLD0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLD0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLD0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCLD0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
