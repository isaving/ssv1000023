package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC020005I struct {
	DcnId			  string
	AcctgAcctNo       string  `valid:"Required;MaxSize(32)"`
	Currency          string  `valid:"Required;MaxSize(10)"`
	TxnAmt            float64 `valid:"Required"`
	TradeDate         string  `valid:"Required;MaxSize(10)"`
	RcrdacctAcctnDt   string  `valid:"Required;MaxSize(10)"`
	RcrdacctAcctnTm   string  `valid:"Required;MaxSize(10)"`
	LiqdDt            string  `valid:"Required;MaxSize(8)"`
	LiqdTm            string  `valid:"Required;MaxSize(8)"`
	TxOrgNo           string  `valid:"Required;MaxSize(4)"`
	AgenOrgNo         string  `valid:"Required;MaxSize(4)"`
	TxTelrNo          string  `valid:"Required;MaxSize(6)"`
	AuthTelrNo        string  `valid:"Required;MaxSize(6)"`
	RchkTelrNo        string  `valid:"Required;MaxSize(6)"`
	LunchChnlTypCd    string  `valid:"Required;MaxSize(4)"`
	AccessChnlTypCd   string  `valid:"Required;MaxSize(4)"`
	TerminalNo        string  `valid:"Required;MaxSize(20)"`
	BizSysNo          string  `valid:"Required;MaxSize(3)"`
	TranCd            string  `valid:"Required;MaxSize(10)"`
	LiqdBizTypCd      string  `valid:"Required;MaxSize(2)"`
	BizKindCd         string  `valid:"Required;MaxSize(3)"`
	BizClsfCd         string  `valid:"Required;MaxSize(20)"`
	DebitCrdtFlg      string  `valid:"Required;MaxSize(1)"`
	MbankFlg          string  `valid:"Required;MaxSize(1)"`
	StateAndRgnCd     string  `valid:"Required;MaxSize(4)"`
	OthbnkBnkNo       string  `valid:"Required;MaxSize(14)"`
	OpenAcctOrgNo     string  `valid:"Required;MaxSize(4)"`
	MbankAcctFlg      string  `valid:"Required;MaxSize(1)"`
	TxDtlTypCd        string  `valid:"Required;MaxSize(1)"`
	CustNo            string  `valid:"Required;MaxSize(14)"`
	MerchtNo          string  `valid:"Required;MaxSize(32)"`
	SvngAcctNo        string  `valid:"Required;MaxSize(32)"`
	CardNoOrAcctNo    string  `valid:"Required;MaxSize(32)"`
	FinTxAmtTypCd     string  `valid:"Required;MaxSize(1)"`
	CashrmtFlgCd      string  `valid:"Required;MaxSize(1)"`
	AcctBal           float64 `valid:"Required"`
	CashTxRcptpymtCd  string  `valid:"Required;MaxSize(1)"`
	FrnexcgStlManrCd  string  `valid:"Required;MaxSize(8)"`
	PostvReblnTxFlgCd string  `valid:"Required;MaxSize(1)"`
	RvrsTxFlgCd       string  `valid:"Required;MaxSize(1)"`
	OrginlTxAcctnDt   string  `valid:"Required;MaxSize(8)"`
	OrginlHostTxSn    string  `valid:"Required;MaxSize(10)"`
	OrginlTxTelrNo    string  `valid:"Required;MaxSize(6)"`
	CustMgrTelrNo     string  `valid:"Required;MaxSize(6)"`
	LoanUsageCd       string  `valid:"Required;MaxSize(4)"`
	AcctBookNo        string  `valid:"Required;MaxSize(15)"`
	PmitRvrsFlg       string  `valid:"Required;MaxSize(1)"`
	ChnlRvrsCtrlFlgCd string  `valid:"Required;MaxSize(4)"`
	GvayLiqdFlg       string  `valid:"Required;MaxSize(1)"`
	MndArapFlg        string  `valid:"Required;MaxSize(1)"`
	CurrPeriodRepayDt string  `valid:"Required"`
}

type AC020005O struct {
	Status	string
	//AcctgAcctNo		string
	//Curreny			string
	//Status			string
	//OpenAcctDt		string
	//Bal				float64

}

type AC020005IDataForm struct {
	FormHead CommonFormHead
	FormData AC020005I
}

type AC020005ODataForm struct {
	FormHead CommonFormHead
	FormData AC020005O
}

type AC020005RequestForm struct {
	Form []AC020005IDataForm
}

type AC020005ResponseForm struct {
	Form []AC020005ODataForm
}

// @Desc Build request message
func (o *AC020005RequestForm) PackRequest(AC020005I AC020005I) (responseBody []byte, err error) {

	requestForm := AC020005RequestForm{
		Form: []AC020005IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020005I",
				},
				FormData: AC020005I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC020005RequestForm) UnPackRequest(request []byte) (AC020005I, error) {
	AC020005I := AC020005I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC020005I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC020005I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC020005ResponseForm) PackResponse(AC020005O AC020005O) (responseBody []byte, err error) {
	responseForm := AC020005ResponseForm{
		Form: []AC020005ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020005O",
				},
				FormData: AC020005O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC020005ResponseForm) UnPackResponse(request []byte) (AC020005O, error) {

	AC020005O := AC020005O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC020005O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC020005O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC020005I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
