package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL166692I struct {
	AcctgAcctNo     string `valid:"Required;MaxSize(32)"` //贷款核算账号
	BeginDt         string `valid:"Required;MaxSize(10)"` //计提开始时间	String(10)  结息优先类型代码1必传（还款分期表的计息开始日期）
	EndDt           string `valid:"Required;MaxSize(10)"` //计提结束时间	String(10)  结息优先类型代码1必传（还款分期表的计息结束日期）
	IntStlPrtyTypCd string `valid:"Required;MaxSize(1)"`  //结息优先类型代码	String(1)  1-以计划还息金额为结息金额
	PeriodNum       int64    `valid:"Required"`             //期数	Numeric(11)  结息优先类型代码1必传
	PlanRpyIntAmt   float64                               //应还利息金额	Numeric(18,2)  结息优先类型代码1必传
	IntDir          string                                //利息方向
}

type IL166692O struct {
	AcctgAcctNo		string
	Records		 	[]IL166692ORecords
}

type IL166692ORecords struct {
	IntrStusCd           string
	Pridnum              int64
	BegintDt             string
	ExpiryDt             string
	PlanRpyintAmt        float64
	ActlRpyintAmt        float64
	AccmCnDwAmt          float64
	AccmIntStlAmt        float64
	RecvblCmpdAmt        float64
	ActlRecvCmpdAmt      float64
	AlrdyTranOffshetIntr float64
	OnshetInt            float64
	WrtoffIntrAmt        float64
	WrtoffIntacrAmt      float64
	RecordStusCd         string
	FrzAmt               float64
	FinlCalcDt           string
	AddCnDwAmt           float64
}


type IL166692IDataForm struct {
	FormHead CommonFormHead
	FormData IL166692I
}

type IL166692ODataForm struct {
	FormHead CommonFormHead
	FormData IL166692O
}

type IL166692RequestForm struct {
	Form []IL166692IDataForm
}

type IL166692ResponseForm struct {
	Form []IL166692ODataForm
}

// @Desc Build request message
func (o *IL166692RequestForm) PackRequest(IL166692I IL166692I) (responseBody []byte, err error) {

	requestForm := IL166692RequestForm{
		Form: []IL166692IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL166692I",
				},
				FormData: IL166692I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL166692RequestForm) UnPackRequest(request []byte) (IL166692I, error) {
	IL166692I := IL166692I{}
	if err := json.Unmarshal(request, o); nil != err {
		return IL166692I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL166692I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL166692ResponseForm) PackResponse(IL166692O IL166692O) (responseBody []byte, err error) {
	responseForm := IL166692ResponseForm{
		Form: []IL166692ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL166692O",
				},
				FormData: IL166692O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL166692ResponseForm) UnPackResponse(request []byte) (IL166692O, error) {

	IL166692O := IL166692O{}

	if err := json.Unmarshal(request, o); nil != err {
		return IL166692O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL166692O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL166692I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
