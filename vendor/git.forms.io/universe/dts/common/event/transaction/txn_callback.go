//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package event

// local transaction confirm request
type AtomicTxnConfirmRequestBody struct {
	RootXid     string `json:"rootXid"`
	ParentXid   string `json:"parentXid"`
	BranchXid   string `json:"branchXid"`
	RequestTime string `json:"requestTime"`
}

type AtomicTxnConfirmRequest struct {
	Head    TxnEventHeader              `json:"head"`
	Request AtomicTxnConfirmRequestBody `json:"request"`
}

// local transaction confirm response
type AtomicTxnConfirmResponseBody struct {
	RootXid      string `json:"rootXid"`
	BranchXid    string `json:"branchXid"`
	ResponseTime string `json:"responseTime"`
}

type AtomicTxnConfirmResponse struct {
	ErrorCode int                          `json:"errorCode"`
	ErrorMsg  string                       `json:"errorMsg"`
	Data      AtomicTxnConfirmResponseBody `json:"data"`
}

// local transaction cancel request
type AtomicTxnCancelRequestBody struct {
	RootXid     string `json:"rootXid"`
	ParentXid   string `json:"parentXid"`
	BranchXid   string `json:"branchXid"`
	RequestTime string `json:"requestTime"`
}

type AtomicTxnCancelRequest struct {
	Head    TxnEventHeader             `json:"head"`
	Request AtomicTxnCancelRequestBody `json:"request"`
}

// local transaction cancel response
type AtomicTxnCancelResponseBody struct {
	RootXid      string `json:"rootXid"`
	BranchXid    string `json:"branchXid"`
	ResponseTime string `json:"responseTime"`
}

type AtomicTxnCancelResponse struct {
	ErrorCode int                         `json:"errorCode"`
	ErrorMsg  string                      `json:"errorMsg"`
	Data      AtomicTxnCancelResponseBody `json:"data"`
}

type AtomicTxnCallbackRequest struct {
	Head    TxnEventHeader               `json:"head"`
	Request AtomicTxnCallbackRequestBody `json:"request"`
}

type AtomicTxnCallbackRequestBody struct {
	RootXid     string `json:"rootXid"`
	ParentXid   string `json:"parentXid"`
	BranchXid   string `json:"branchXid"`
	RequestTime string `json:"requestTime"`
}
