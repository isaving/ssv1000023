//Version: v1.0.0
package controllers

import (
	constants "git.forms.io/isaving/sv/ssv1000023/constant"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000023/services"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000023Controller struct {
    controllers.CommController
}

func (*Ssv1000023Controller) ControllerName() string {
	return "Ssv1000023Controller"
}

// @Desc ssv1000023 controller
// @Author
// @Date 2020-12-12
func (c *Ssv1000023Controller) Ssv1000023() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000023Controller.Ssv1000023 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000023I := &models.SSV1000023I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv1000023I); err != nil {
		c.SetServiceError(errors.New(err, constants.ERRCODE2))
		return
	}
  if err := ssv1000023I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000023 := &services.Ssv1000023Impl{} 
    ssv1000023.New(c.CommController)
	ssv1000023.SSV1000023I = ssv1000023I

	ssv1000023O, err := ssv1000023.Ssv1000023(ssv1000023I)

	if err != nil {
		log.Errorf("Ssv1000023Controller.Ssv1000023 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv1000023O.PackResponse()
	if err != nil {
		c.SetServiceError(errors.New(err, constants.ERRCODE1))
		return
	}
	c.SetAppBody(responseBody)
}
// @Title Ssv1000023 Controller
// @Description ssv1000023 controller
// @Param Ssv1000023 body models.SSV1000023I true body for SSV1000023 content
// @Success 200 {object} models.SSV1000023O
// @router /create [post]
func (c *Ssv1000023Controller) SWSsv1000023() {
	//Here is to generate API documentation, no need to implement methods
}
