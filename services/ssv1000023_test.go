package services

import (
	"errors"
	"fmt"
	"git.forms.io/isaving/models"
	jsoniter "github.com/json-iterator/go"
	. "reflect"
	"testing"
)

//这个要改成service的结构
func (this *Ssv1000023Impl) RequestSyncServiceWithDCN(dstDcn, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

//这个要改成service的结构
func (this *Ssv1000023Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

func (this *Ssv1000023Impl) RequestServiceWithDCN(dstDcn, serviceKey string, requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
	responseData = []byte(`{"Code":0,"Data":{"BS-LEGO":["C03101"]},"Message":"","errorCode":0,"errorMsg":"","response":{"BS-LEGO":["C03101"]}}`)
	return
}

func TestService(t *testing.T) {
	runTest(Ssv1000023Impl{}, "Ssv1000023") //这两个都要改哦
}

//报文头
var SrcAppProps = map[string]string{
	"TxUserId":   "1001",
	"TxDeptCode": "1001",
}

//输入请求
var request = []models.SSV1000023I{
	//{
	//	Algorithm: "rsa",
	//},
	//{
	//	Algorithm: "rsa",
	//},
}


//使用反引号可以直接换行 还不会被带双引号影响
var response = map[string]interface{}{
	//这个key 是你调用RequestSyncServiceElementKey()这个函数传的serviceKey的字符串 不一定是topic 好好看看自己的代码传的字符串是啥
	//可以直接粘贴报文 也可以写一个返回结构体

	"KmsGetRotateKeyWithCrypto":`{    "Code":0,    "Message":"O.K.",    "Data":{        "public_pem":"LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUF4aHUzYVd5cW1VMmxiWDB6c0ZidApwOE1LTG85QjdKK2hBd1MzYVdreXNKTHZrZHZ0djArR3NLK0gya1NkVkRQVms5aEtYUFFlL2hxYUM5dlJZaXYrCmYzS1kzRTRWMkI5dmhMKzdBK0ZNc2oyK2M4SkFkTk9wYmp2YVJmQldLcWFlbWNvZE00aWdZNlQ4MkFLSHk0eW4KUUNnOVFlYjlGSWFFNG1kRmEwLzhNTjVqeko2THhFMEZRbk1aRjgvVjNHYm4xbU9XYzBoTmdaRnVUNmNaUE5xUwpxNkhiTjdBMTlpRFpySlVManRidzA0MHdTa1lUWDFhVG5GMjR6LzZNUFRXZ2ltZEJqZCtxUXhzcitNT1ZjZWZKCm5wNEdHcWEvdnZsWUlDRExyTnhmRGJIRmVHbk9DNitlS3EwUjZvNUhCTDRFSklmQ29oQ1NLTGcwUFdHbGtRWkYKTHdJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg=="    }}`,
	/*"DAACRTC3": models.DAACRTC3O{
		LogTotCount: "1",
		Records: []models.DAACRTC3ORecords{{
			AccmCmpdAmt:    0,
			AccmIntSetlAmt: 0,
			AccmWdAmt:      0,
			AcctgAcctNo:    "",
			AcruUnstlIntr:  0,
			RecordNo:       0,
			RepaidInt:      0,
			RepaidIntAmt:   0,
			Status:         "",
			UnpaidInt:      0,
			UnpaidIntAmt:   0,
		}},
	},

	"DAILUFK1": `{"Form":[{"FormData":{"State":"ok"}}]}`,*/
}

//func (this *Ssv1000023Impl)RequestServiceWithDCN(dstDcn, serviceKey string,requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
//	if serviceKey == "DlsuDcnLists" {
//
//	}
//}

var errServiceKey []string

//初始化
func init() {
	for serviceKey := range response {
		errServiceKey = append(errServiceKey, serviceKey)
	}
}

var currentIndex = 0
var passed bool

//排列组合返回报文
func RequestService(serviceKey string, reqBytes []byte) (responseData []byte, err error) {

	if serviceKey == errServiceKey[currentIndex] && passed {
		currentIndex++
		if currentIndex >= len(errServiceKey) {
			currentIndex = 0
		}
		return nil, errors.New("error")
	}
	switch response[serviceKey].(type) {
	case string:
		responseData = []byte(response[serviceKey].(string))
	default:
		responseData = getByte(response[serviceKey])
	}
	return
}

func getByte(v interface{}) (bt []byte) {
	bt, _ = jsoniter.Marshal(struct {
		Form [1]struct{ FormData interface{} }
	}{Form: [1]struct{ FormData interface{} }{{v}}})
	return
}

func runTest(serviceStruct interface{}, method string) {
	param := make([]Value, 1)
	for _, req := range request {
		param[0] = ValueOf(&req)
		passed = false
		for i := 0; i < len(response); i++ {
			callMethod(serviceStruct, method, param)
			passed = true
		}
	}
}

func callMethod(serviceStruct interface{}, method string, params []Value) {
	service := New(TypeOf(serviceStruct))
	service.Elem().FieldByName("SrcAppProps").Set(ValueOf(SrcAppProps))
	ret := service.MethodByName(method).Call(params)
	fmt.Printf(" 返回 [ %v ] \n 错误 [ %v ]\n\n", ret[0], ret[1])
}
