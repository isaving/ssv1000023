//Version: v1.0.0
package services

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv1000023/constant"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	"git.forms.io/universe/common/json"
	"git.forms.io/universe/solapp-sdk/log"
)
type Ssv1000023Impl struct {
    services.CommonService
	//TODO ADD Service Self Define Field
	SSV1000023O         *models.SSV1000023O
	SSV1000023I         *models.SSV1000023I

	PublicPem			string
}
// @Desc Ssv1000023 process
// @Author
// @Date 2020-12-12
func (impl *Ssv1000023Impl) Ssv1000023(ssv1000023I *models.SSV1000023I) (ssv1000023O *models.SSV1000023O, err error) {

	impl.SSV1000023I = ssv1000023I
	var Flag string
	if "" == constants.PublicPem {
		//get kms PublicPem
		if err := impl.KmsGetPublicPem(); nil != err {
			return nil, err
		}
		constants.PublicPem = impl.PublicPem
		Flag = "1"
	} else {
		impl.PublicPem = constants.PublicPem
		Flag = "2"
	}
	SSV1000023O := &models.SSV1000023O{
		PublicPem: impl.PublicPem,
	}
	log.Infof("Flag:%v, public_pem:%v", Flag, impl.PublicPem)

	return SSV1000023O, nil
}

func (impl *Ssv1000023Impl) KmsGetPublicPem() error {
	makeRes := make(map[string]interface{})
	makeRes["Algorithm"] = "rsa"
	rspBody, err := json.Marshal(makeRes)
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.KmsGetPublicPem, rspBody);
	if err != nil{
		log.Errorf("KmsGetPublicPem, err:%v",err)
		return errors.New(err, constants.ERRCODE4)
	}
	log.Debugf("resBody:%v",string(resBody))

	CommResp := &CommResp{}
	if err := json.Unmarshal(resBody, &CommResp); nil != err {
		return errors.New(err, constants.ERRCODE2)
	}

	if 0 != CommResp.Code {
		log.Error(CommResp.Message)
		return errors.New(CommResp.Message, constants.ERRCODE4)
	}
	impl.PublicPem = CommResp.Data.PublicPem
	return nil
}

type CommResp struct{
	Code	int
	Message	string
	Data	struct{
		PublicPem	string	`json:"public_pem"`
	}
}

